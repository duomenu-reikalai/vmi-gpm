# Kontekstas
Kontekstas aprašytas [Duomenų reikalų Facebook poste](https://www.facebook.com/duomenureikalai/posts/pfbid057ayqBQ18z1doxTn9CJLaVDLJXex9o4ivkaaGvELic1jsgczLn4xMZTSgzKZE1U4l)

# Kaip atsisiųsti
Siųskitės visą repozitoriją, o ne atskirus failus. Kitu atveju Gitlab puslapis pakimba.
